// 1 eleminate duplicates in array

function eliminateDuplicates(arr) {
  const newArray = [];
  for (let i = 0; i < arr.length; i++) {
    if (!newArray.includes(arr[i])) {
      newArray.push(arr[i]);
    }
  }
  return newArray;
}

const myArray = [3, 4, 9, 3, 8, 0, 4, 9];

console.log(eliminateDuplicates(myArray));

// find largest and smallest

function findLargestAndSmallest(arr) {
  arr.sort((a, b) => a - b);

  return [arr[0], arr[arr.length - 1]];
}

const myArray1 = [3, 4, 9, 3, 8, 0, 4, 9];

console.log(findLargestAndSmallest(myArray1));

// reverse string

function reverseString(str) {
  return str.split('').reverse().join('');
}

const string = "KRIYATMA";
console.log(reverseString(string));

//find longest word in sentence

function longestWord(sentence) {
  const words = sentence.split(" ");

  words.sort((a, b) => b.length - a.length);
  
    return words[0];

}

const sentence = "the quick brown fox jumps over the lazy dog";

console.log(longestWord(sentence));


